package app;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
public class CarSaleController {

    @RequestMapping("/")
    String render() {
        return "Welcome to The Purple Car, an online car sale company";
    }

    @RequestMapping("/add")
    public String render(@RequestBody Car car, HttpServletResponse response) {
        if (StringUtils.isEmpty(car.getcarID())) {
            response.setStatus(400);
            return "Input error. You must specify a Car ID.";
        }

        if (StringUtils.isEmpty(car.getvin())) {
            response.setStatus(400);
            return "Input error. You must specify a VIN.";
        }
        if (car.getvin().length() > 5) {
            response.setStatus(400);
            return "Input error. Content length must be between 1 and 5 chars.";
        }
        if (!car.getvin().matches("[a-zA-Z0-9]+?")) {
            response.setStatus(400);
            return "Input error. Invalid VIM format";
        }

        if (StringUtils.isEmpty(car.getmodel())) {
            response.setStatus(400);
            return "Input error. You must specify a model.";
        }
        if (car.getmodel().length() > 3) {
            response.setStatus(400);
            return "Input error. Content length must be between 1 and 3 chars.";
        }
        if (!car.getmodel().matches("[a-zA-Z0-9]+?")) {
            response.setStatus(400);
            return "Input error. Invalid model format";
        }

        return "Added car: " + car.toString();
    }
}
