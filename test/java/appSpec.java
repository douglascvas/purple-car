package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.CoreMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonParseException;

import java.io.IOException;

@DisplayName("Usability unit tests")
@WebMvcTest
public class appSpec {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void should_outputCarID_whenValidRequest()
            throws JsonParseException, IOException, Exception {
        Car car = new Car(1, "1", "foo");
        ObjectMapper mapper = new ObjectMapper();
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(car))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("1")));
    }

    @Test
    public void should_outputError_whenIdIsNull()
            throws JsonParseException, IOException, Exception {
        Car car = new Car(null, "1", "foo");
        ObjectMapper mapper = new ObjectMapper();
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(car))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(containsString("Input error. You must specify a Car ID.")));
    }

    @Test
    public void should_outputError_whenVinIsNull()
            throws JsonParseException, IOException, Exception {
        Car car = new Car(1, null, "foo");
        ObjectMapper mapper = new ObjectMapper();
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(car))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(containsString("Input error. You must specify a VIN.")));
    }

    @Test
    public void should_outputError_whenModelIsNull()
            throws JsonParseException, IOException, Exception {
        Car car = new Car(1, "1", null);
        ObjectMapper mapper = new ObjectMapper();
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(car))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(containsString("Input error. You must specify a model.")));
    }

    @Test
    public void should_outputError_whenVinLengthIsTooBig()
            throws JsonParseException, IOException, Exception {
        Car car = new Car(1, "1111111111", "foo");
        ObjectMapper mapper = new ObjectMapper();
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(car))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(containsString("Input error. Content length must be between 1 and 5 chars.")));
    }

    @Test
    public void should_outputError_whenVinSyntaxIsInvalid()
            throws JsonParseException, IOException, Exception {
        Car car = new Car(1, "$$$%^", "foo");
        ObjectMapper mapper = new ObjectMapper();
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(car))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(containsString("Input error. Invalid VIM format")));
    }

    @Test
    public void should_outputError_whenModelLengthIsTooBig()
            throws JsonParseException, IOException, Exception {
        Car car = new Car(1, "1", "123456789");
        ObjectMapper mapper = new ObjectMapper();
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(car))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(containsString("Input error. Content length must be between 1 and 3 chars.")));
    }

    @Test
    public void should_outputError_whenModelSyntaxIsInvalid()
            throws JsonParseException, IOException, Exception {
        Car car = new Car(1, "1", "$%^");
        ObjectMapper mapper = new ObjectMapper();
        this.mockMvc.perform(post("/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(car))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(containsString("Input error. Invalid model format")));
    }
}
